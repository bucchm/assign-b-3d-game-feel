﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovement : MonoBehaviour
{
    Vector3 jump;
    float jumpForce = 3f;
    public bool isGrounded;
    public Rigidbody rb;


    Transform thisTrans;
	Vector3 originalPosition;
    [SerializeField]
    Transform camera;
	public Transform levelEnd;

    //public GameObject dustPuff;
    public ParticleSystem dustParticle;

    // Use this for initialization
    void Start()
    {
        thisTrans = this.transform;
		originalPosition = gameObject.transform.position;
		rb = GetComponent<Rigidbody> ();
        jump = new Vector3(0.0f, 2.0f, 0.0f);

    }

    int count = 0;
    void OnCollisionStay()
    {
        isGrounded = true;
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Space) && isGrounded)
        {

            rb.AddForce(jump * jumpForce, ForceMode.Impulse);
            isGrounded = false;
        }

        //rb.AddForce (rb.velocity.normalized * 0.01f, ForceMode.Impulse);

        if (thisTrans.position.y <= 0){
			rb.velocity = new Vector3 (0, 0, 0);
			thisTrans.position = originalPosition;
		}
        //thisTrans.position += camera.forward * Input.GetAxis("Vertical")/8;

    }

    private void OnCollisionEnter(Collision collision)
    {
        //GameObject dustObject = Instantiate(dustPuff, thisTrans.position, thisTrans.rotation);
        dustParticle.GetComponent<ParticleSystem>().Emit(20);
        dustParticle.transform.position = thisTrans.position + new Vector3(0, 0.1f, 0);
        //dustParticle.transform.rotation = new Vector3(0, 0, 0);
    }


    void OnTriggerEnter(Collider other) {
        if (other.gameObject.CompareTag("levelEnd")) {
        Debug.Log("touch");
        rb.velocity = new Vector3(0, 0, 0);
        thisTrans.position = originalPosition;
        }
	}
}
