﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;

public class UI : MonoBehaviour {

    public int score;

    Text text;



    void Awake()
    {
        text = GetComponent<Text>();
        score = 0;
    }

    private void Update()
    {
        text.text = "Score:" + score;
    }
}
