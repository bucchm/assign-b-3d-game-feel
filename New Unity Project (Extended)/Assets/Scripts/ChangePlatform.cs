﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChangePlatform : MonoBehaviour {

    public Transform Level1;
	public Transform Level2;
	public Transform Level3;
	public Transform Level4;

    public GameObject[] level3Cylinder1;
    public GameObject[] level3Cylinder2;
    public Transform Cylinder1Pivot;
    public Transform Cylinder2Pivot;

    public GameObject[] level4Top;
    public GameObject[] level4Bottom;
    public Transform LevelTopPivot;
    public Transform LevelBottomPivot;

    public int change = 0;


	// Use this for initialization
	void Start () {
		change = 0; //Debug check

        level3Cylinder1 = GameObject.FindGameObjectsWithTag("CylinderFront");
        level3Cylinder2 = GameObject.FindGameObjectsWithTag("CylinderBack");

        level4Top = GameObject.FindGameObjectsWithTag("LevelTop");
        level4Bottom = GameObject.FindGameObjectsWithTag("LevelBottom");

        Level1.gameObject.SetActive (true);
        Level2.gameObject.SetActive (true);
        Level3.gameObject.SetActive (true);
        Level4.gameObject.SetActive (true);
    }
	
	// Update is called once per frame
	void Update () {
		//Debug.Log (change);
		if (change > 3) {
			change = 0;
		}

		if (change == 0) {
            transform.position = new Vector3(3.79f, 13.85f, 11.73f);

            Level1.gameObject.SetActive (true);
			Level2.gameObject.SetActive (false);
			Level3.gameObject.SetActive (false);
			Level4.gameObject.SetActive (false);
		}

		if (change == 1) {
            transform.position = new Vector3(-15.71f, 8.54f, -5.86f);

            Level1.gameObject.SetActive (false);
			Level2.gameObject.SetActive (true);
			Level3.gameObject.SetActive (false);
			Level4.gameObject.SetActive (false);
		}

		if (change == 2) {
            transform.position = new Vector3(2.72f, 11.6f, -14.75f);

            Level1.gameObject.SetActive (false);
			Level2.gameObject.SetActive (false);
			Level3.gameObject.SetActive (true);
			Level4.gameObject.SetActive (false);

            /*for (int i = 0; i < level3Cylinder1.Length; i++)
            {
                level3Cylinder1[i].transform.RotateAround(Cylinder1Pivot.position, Vector3.back, 20 * Time.deltaTime);
            }

            for (int i = 0; i < level3Cylinder2.Length; i++)
            {
                level3Cylinder2[i].transform.RotateAround(Cylinder2Pivot.position, Vector3.forward, 20 * Time.deltaTime);
            }*/
        }

		if (change == 3) {
            transform.position = new Vector3(-15.85f, 13.29f, 1.29f);

            Level1.gameObject.SetActive (false);
			Level2.gameObject.SetActive (false);
			Level3.gameObject.SetActive (false);
			Level4.gameObject.SetActive (true);

            /*for (int i = 0; i < level4Top.Length; i++)
            {
                level4Top[i].transform.RotateAround(LevelTopPivot.position, Vector3.up, 20 * Time.deltaTime);
            }

            for (int i = 0; i < level4Bottom.Length; i++)
            {
                level4Bottom[i].transform.RotateAround(LevelBottomPivot.position, Vector3.down, 20 * Time.deltaTime);
            }
            */
        }

        //Level4.transform.RotateAround(Level4.position, Vector3.up, 20 * Time.deltaTime);
    }

	void OnTriggerEnter(Collider player){
		if (player.transform.tag == "Player") {
			change += 1; 
			Debug.Log("Change");
		}

	}
}
