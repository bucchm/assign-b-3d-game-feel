﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlatformRotation : MonoBehaviour {

    public Transform followCamera;

     float rotationSpeed = 30.0f;
     int maxRotation = 30;
     float lerpBack_speed = 0.02f;

    private Quaternion startingRotation = Quaternion.Euler(0, 0, 0);
    private Vector3 x, z;

    void Update()
    {
        x = Input.GetAxis("Vertical")/3 * Time.deltaTime * followCamera.right;
        z = Input.GetAxis("Horizontal")/3 * Time.deltaTime * followCamera.forward;
    }
    void FixedUpdate()
    {
        transform.Rotate((x - z) * rotationSpeed);

        transform.rotation = Quaternion.Lerp(transform.rotation, startingRotation, lerpBack_speed);
    }
}

/*    public Transform followCamera;
    private Quaternion startingRotataion = Quaternion.Euler(0, 0, 0);

    // Use this for initialization
    void Start () {

    }
	
	// Update is called once per frame
	void Update () {
        transform.rotation *= Quaternion.Euler (Input.GetAxis ("Vertical") / 6, 0, -Input.GetAxis ("Horizontal") / 3);
        transform.rotation = Quaternion.Slerp(transform.rotation, Quaternion.Euler(0, 0, 0), 0.01f);

    }
*/