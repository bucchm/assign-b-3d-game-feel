﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MinimapFollow : MonoBehaviour {

    Transform thisTrans;

    [SerializeField]
    Transform player;

    // Use this for initialization
    void Start () {
        thisTrans = this.transform;
    }

    // Update is called once per frame
    void Update () {
        thisTrans.position = player.position;
    }
}
