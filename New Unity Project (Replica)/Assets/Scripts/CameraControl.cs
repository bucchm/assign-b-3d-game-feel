﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraControl : MonoBehaviour {
    Transform thisTrans;
    public Vector3 currentRotation;
    public float correctRotation = 0.0f;
    public float smoothness = 5.0f;

    [SerializeField]
    Transform player;

    // Use this for initialization
    void Start () {
        thisTrans = this.transform;
    }
	
	// Update is called once per frame
	void Update () {
        if (Input.GetKey("escape"))
            Application.Quit();

        thisTrans.position = player.position;
        if((thisTrans.forward.y > 0.0f) && (thisTrans.forward.y < -0.7f))
        {
            currentRotation = transform.eulerAngles;
            currentRotation.z = Mathf.Lerp(currentRotation.z, correctRotation, Time.deltaTime * smoothness);
            transform.eulerAngles = currentRotation;
        }
		thisTrans.rotation = Quaternion.Euler
			(-30 * ((2 * (Input.mousePosition.y / Screen.height) - 1)), 180 * ((2 * (Input.mousePosition.x / Screen.width) - 1)), 0);


    }
}